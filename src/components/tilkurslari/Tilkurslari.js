import uch1 from "../Diyorbek/section1_svg/uch 1.png";
import uch2 from "../Diyorbek/section1_svg/uch 2.png"
import uch3 from "../Diyorbek/section1_svg/uch 3.png"
import uch4 from "../Diyorbek/section1_svg/uch 4.png"
import sana from "../Diyorbek/section1_svg/sana.svg"
import qumsoat from "../Diyorbek/section1_svg/qumsoat.svg"
import strelka from "../Diyorbek/section1_svg/strelka (3).svg"
import qongiroq from "../Diyorbek/section1_svg/qong'iroq.svg"
import ptichka from "../Diyorbek/section1_svg/ptichka.svg"
import katnay from "../Diyorbek/section1_svg/karnay.svg"
import dumaloq from "../Diyorbek/section1_svg/dumaloq.svg"
import idumaloq from "../Diyorbek/section1_svg/idumaloq.svg"
import togri from "../Diyorbek/section1_svg/togri.svg"
import idum from "../Diyorbek/section1_svg/idum.svg"
import i2 from "../Diyorbek/section1_svg/i2.svg"
import belgi from "../Diyorbek/section1_svg/belgi.svg"
import cat from '../../assets/images_gif/ezgif.com-gif-maker.gif'
import React from 'react'

const Tilkurslari = () => {
  return (
    <section id="tilkurslari" className="w-full h-auto pb-10 flex">
      <div>
        <div className="tag  flex justify-center mt-10">
          <h1>Xorijiy til</h1>
        </div>
        <div className="tag flex justify-center pt-5 gap-4 items-center -mt-[100px]">
          <img className="" src={cat} alt="" />
          <h1>kurslari</h1>
        </div>

        <div className="grid gap-4 grid-cols-2 grid-rows-2 bg mt-[170px]  ml-[118px] mr-[118px]">
          <div
            className="rounded-[20px] bg-[#FAFAFA] truncate"
            data-aos="flip-left"
            data-aos-easing="ease-out-cubic"
            data-aos-duration="2000"
          >
            <div className="flex ">
              <div className="m-10">
                <h2 className="font-bold text-3xl">Ingliz tili kursi</h2>
                <p>Tanishish menyusi</p>
              </div>
              <img
                className="ml-[56px] w-17 h-30 mt-10 rounded-xl"
                src={uch1}
                alt="#"
              />
            </div>

            <div className="mt-[54px] ml-10">
              <div className="flex">
                <img src={sana} alt="#" />
                <p className="ml-[10px] mt-[19px]">Dars kunlari:</p>
                <h6 className="ml-[155px] mt-[21px]">Haftada 3 marotaba</h6>
              </div>

              <div className="flex">
                <img className="-mt-4" src={qumsoat} alt="#" />
                <p className="ml-[10px]">Dars davomiyligi:</p>
                <h6 className="mt-1 ml-[121px]">90 minut</h6>
              </div>

              <div className="flex">
                <img className="-mt-[14px]" src={strelka} alt="#" />
                <p className="ml-[10px]">Intensiv kurs:</p>
                <h6 className="mt-[3px] ml-[150px]">2 oy</h6>
              </div>

              <div className="flex">
                <img className="-mt-4" src={qongiroq} alt="#" />
                <p className="ml-[10px]">Har bir bosqich davomiyligi:</p>
                <h6 className="mt-[3px] ml-11">3 oy</h6>
              </div>

              <div className="flex -m-1 ml-0">
                <img className="-mt-4" src={ptichka} alt="#" />
                <p className="ml-[10px]">IELTS tayyorlov kursi:</p>
                <h6 className="mt-[3px] ml-[95px]">
                  Boshlang'ich tayyorlov 2 oy
                </h6>
              </div>

              <button className="oibtn flex -mt-8">
                Kursga yozilish
                <img className="-m-1 ml-2" src={belgi} alt="#" />
              </button>
            </div>
          </div>

          <div
            className="rounded-[20px] bg-[#FAFAFA] truncate"
            data-aos="flip-right"
            data-aos-easing="ease-out-cubic"
            data-aos-duration="2000"
          >
            <div className="flex ">
              <div className="m-10 ">
                <h2 className="font-bold text-3xl">Koreys tili kursi</h2>
                <p>Tanishish menyusi</p>
              </div>
              <img
                className="ml-[29px] w-17 h-30 mt-10 rounded-xl"
                src={uch2}
                alt="#"
              />
            </div>
            <div className="mt-[70px] ml-10">
              <div className="flex">
                <img src={sana} alt="#" />
                <p className="ml-[10px] mt-[19px]">Dars kunlari:</p>
                <h6 className="ml-[155px] mt-[21px]">Haftada 3 marotaba</h6>
              </div>

              <div className="flex">
                <img className="-mt-2" src={qumsoat} alt="#" />
                <p className="ml-[10px]">Dars davomiyligi:</p>
                <h6 className="mt-1 ml-[121px]">90 minut</h6>
              </div>

              <div className="flex">
                <img className="-mt-[5px]" src={dumaloq} alt="#" />
                <p className="ml-[10px]">1 oylik darslar soni:</p>
                <h6 className="mt-[3px] ml-[106px]">12 ta</h6>
              </div>

              <div className="flex">
                <img className="-mt-2" src={qongiroq} alt="#" />
                <p className="ml-[10px]">Har bir bosqich davomiyligi:</p>
                <h6 className="mt-[3px] ml-11">3 oy</h6>
              </div>
              <button className="oibtn flex mt-4">
                Kursga yozilish
                <img className="-m-1 ml-2" src={belgi} alt="#" />
              </button>
            </div>
          </div>

          <div
            className="rounded-[20px] bg-[#FAFAFA] w-auto h-auto h-[570px; truncate] "
            data-aos="flip-left"
            data-aos-easing="ease-out-cubic"
            data-aos-duration="2000"
          >
            <div className="flex ">
              <div className="m-10">
                <h2 className="font-bold text-3xl">Rus tili kursi</h2>
                <p>Tanishish menyusi</p>
              </div>
              <img
                className="ml-[73px] w-17 h-30 mt-10 rounded-xl"
                src={uch3}
                alt="#"
              />
            </div>

            <div className="mt-[54px] ml-10">
              <div className="flex">
                <img src={sana} alt="#" />
                <p className="ml-[10px] mt-[19px]">Dars kunlari:</p>
                <h6 className="ml-[155px] mt-[21px]">Haftada 3 marotaba</h6>
              </div>

              <div className="flex">
                <img className="-mt-4" src={qumsoat} alt="#" />
                <p className="ml-[10px]">Dars davomiyligi:</p>
                <h6 className="mt-1 ml-[121px]">90 minut</h6>
              </div>

              <div className="flex">
                <img className="-mt-[14px]" src={strelka} alt="#" />
                <p className="ml-[10px]">Intensiv kurs:</p>
                <h6 className="mt-[3px] ml-[150px]">2 oy</h6>
              </div>

              <div className="flex">
                <img className="-mt-4" src={qongiroq} alt="#" />
                <p className="ml-[10px]">Har bir bosqich davomiyligi:</p>
                <h6 className="mt-[3px] ml-11">3 oy</h6>
              </div>

              <div className="flex">
                <img className="-mt-4" src={katnay} alt="#" />
                <p className="ml-[10px]">Mustaqil so'zlashuv:</p>
                <h6 className="mt-[3px] ml-[101px]">3 oy</h6>
              </div>

              <button className="oibtn flex mt-4">
                Kursga yozilish
                <img className="-m-1 ml-2" src={belgi} alt="#" />
              </button>
            </div>
          </div>

          <div
            className="rounded-[20px] bg-[#FAFAFA] truncate"
            data-aos="flip-left"
            data-aos-easing="ease-out-cubic"
            data-aos-duration="2000"
          >
            <div className="flex ">
              <div className="m-10">
                <h2 className="font-bold text-3xl">
                  Nega aynan <br />
                  Biz?
                </h2>
              </div>
              <img
                className="ml-[76px] w-17 h-30 mt-10 rounded-xl"
                src={uch4}
                alt="#"
              />
            </div>
            <p className=" w-[300px] ml-10 mt-14">
              English Campus oʻquv markazi oʻquvchilarini shunchaki kelib oʻqib
              ketishlarini emas, balki natijaga erishishlarining tarafdoridir!
              Biz sizga natijaga erishishingizni kafolatlaymiz va siz bunga
              albatta erishasiz!
            </p>
            <button className="oibtn flex  m-5  ml-[50px] mt-[50px] w-[500px]">
              Kursga yozilish
              <img className="-m-1 ml-2" src={belgi} alt="#" />
            </button>
          </div>
        </div>

        <div>
          <div className="grid gap-4 grid-cols-2 grid-rows-2 bg mt-[80px]  ml-[118px] mr-[118px]">
            <div
              className="rounded-[20px] bg-[#FAFAFA] truncate"
              data-aos-duration="2000"
              data-aos="zoom-in"
            >
              <div className="flex ">
                <div className="m-10">
                  <h2 className="text-2xl  font-bold">
                    Mavjud <br />
                    mumammolar
                  </h2>
                </div>
                <img
                  className=" w-27 h-40 mt-10 h-[60px] ml-[155px] mt-[50px]"
                  src={idumaloq}
                  alt="#"
                />
              </div>

              <div className="mt-[54px] ml-10">
                <div className="flex">
                  <img src={idum} alt="#" />
                  <p className="ml-[10px] mt-[19px]">
                    Til o’rganishdagi dangasalikni yenga olmaslik
                  </p>
                </div>

                <div className="flex">
                  <img className="-mt-4" src={idum} alt="#" />
                  <p className="ml-[10px]">Sifatsiz ta’lim beruvchi ustozlar</p>
                </div>

                <div className="flex">
                  <img className="-mt-[14px]" src={idum} alt="#" />
                  <p className="ml-[10px]">Zerikarli darslar</p>
                </div>

                <div className="flex">
                  <img className="-mt-4" src={idum} alt="#" />
                  <p className="ml-[10px]">Noaniq yo’nalishda dars berilishi</p>
                </div>

                <div className="flex">
                  <img className="-mt-4" src={idum} alt="#" />
                  <p className="ml-[10px]">
                    Xorijiy tilni o’rganib bo’lish uchun, aniq vaqt
                    qo’yilmasligi
                  </p>
                </div>
              </div>
            </div>
            <div
              className="rounded-[20px] bg-[#FAFAFA] truncate"
              data-aos-duration="2000"
              data-aos="zoom-in"
            >
              <div className="flex ">
                <div className="m-10">
                  <h2 className="text-2xl  font-bold">
                    Muammolarning <br />
                    bizdagi yechimi
                  </h2>
                </div>
                <img
                  className=" w-27 h-40 mt-10 h-[60px] ml-[155px] mt-[50px]"
                  src={togri}
                  alt="#"
                />
              </div>

              <div className="mt-[54px] ml-10 ">
                <div className="flex ">
                  <img src={i2} alt="#" />
                  <p className="ml-[10px] mt-[19px]   ">
                    Xorijiy tilni o’rganish davridagi o’zini qo’lga olish 
                  </p>
                </div>

                <div className="flex">
                  <img className="-mt-4" src={i2} alt="#" />
                  <p className="ml-[10px]">
                    O’zbekiston bo’ylab
                    mutaxassislardan bilim olish imkoni
                  </p>
                </div>

                <div className="flex">
                  <img className="-mt-[14px]" src={i2} alt="#" />
                  <p className="ml-[10px]">
                    Aniq metodikalar asosida Intensiv darsliklar ketma-ketligi
                  </p>
                </div>

                <div className="flex mb-[66px]">
                  <img className="-mt-4" src={i2} alt="#" />
                  <p className="ml-[10px]">
                    Belgilangan muddat ichida xorijiy tilni oson o’rganish
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Tilkurslari
