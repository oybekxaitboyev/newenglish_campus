import React from 'react'
import cat from '../../assets/images_gif/ezgif.com-gif-maker.gif'
import idumaloq from "../Diyorbek/section1_svg/idumaloq.svg"
import togri from "../Diyorbek/section1_svg/togri.svg"
import idum from "../Diyorbek/section1_svg/idum.svg"
import i2 from "../Diyorbek/section1_svg/i2.svg"
import girl1 from '../../assets/little-school-girl-with-notebook-rucksack-isolated-background 1.png'
import ingliz from '../bolalarkurslari/pictures/🇬🇧.svg'
import rus from "../bolalarkurslari/pictures/🇷🇺.svg";
import xusnixat from "../bolalarkurslari/pictures/✍️.svg";
import matem from "../bolalarkurslari/pictures/🧮.svg";
import estetik from "../bolalarkurslari/pictures/🪴.svg";
import qaychi from "../bolalarkurslari/pictures/✂️.svg";
import belgi from "../Diyorbek/section1_svg/belgi.svg"


const Bolalarkurslari = () => {
  return (
    <section id="bolalarkurslari" className="w-full h-auto pb-20 flex">
      <div>
        <div className="tag mt-[200px] flex justify-center">
          <h1>Bolalar</h1>
        </div>
        <div className="tag flex justify-center gap-4 items-center -mt-[70px]">
          <img className="" src={cat} alt="" />
          <h1>kurslari</h1>
        </div>

        <div className="part2">
          <div className="asd" data-aos="flip-right" data-aos-duration="1500">
            <div className="sarlavha1">
              <h1 className="sarlavha2">Bolalar kurslari</h1>

              <p className="sarlavha3">Tanishish menyusi </p>
            </div>

            <div className="mainlanguages">
              <div className="languagecourse">
                <p>
                  <img src={ingliz} />
                  Ingliz tili
                </p>
                <p>
                  {" "}
                  <img src={rus} /> Rus tili
                </p>
                <p>
                  {" "}
                  <img src={xusnixat} />
                  Husnixat
                </p>
              </div>

              <div className="languagecourse">
                <p>
                  <img src={matem} />
                  Matematika
                </p>
                <p>
                  {" "}
                  <img src={estetik} /> Etika-Estetika
                </p>
                <p>
                  {" "}
                  <img src={qaychi} />
                  Mehnat darsi
                </p>
              </div>
            </div>
            <button className="languagebutton">
              {" "}
              Kursga yozilish <img src={belgi} />
            </button>
          </div>

          <img
            className="girl"
            src={girl1}
            alt=""
            data-aos="flip-left"
            data-aos-duration="1500"
          />
        </div>

        <div
          className="gavb"
          data-aos="fade-up"
          data-aos-anchor-placement="top-bottom"
        >
          <div className="glav2" data-aos="flip-left">
            <h2 className="what">Nega aynan Biz?</h2>

            <div className="part3">
              <div>
                <p className="engcs">
                  English Campus oʻquv markazi tashkil etilgan bolalar kurslari
                  orqali farzandlarimiz, nafaqat yuqoridagi yoʻnalishlar
                  boʻyicha bilim koʻnikmalarga ega boʻladi, balki Muomala
                  madaniyati, Dunyo qarashining kengayishi va tengdoshlariga
                  nisbatan maʼlumotlarni oson qabul qilish qobiliyatlarini
                  shakllantiradi.
                </p>
              </div>

              <div>
                <p className="engcs" data-aos="flip-right">
                  {" "}
                  Bizdagi malakali mutaxassislar esa farzandingiz tarbiyali,
                  odobli, aqlli inson boʻlib yetishishlari uchun oʻzlaridagi bor
                  imkoniyatni ishga soladilar!
                </p>
              </div>
            </div>
            <br />
            <br />

            <button className="languagebuttons">
              {" "}
              Kursga yozilish <img src={belgi} />
            </button>
          </div>
        </div>

        <div className=" grid gap-4 grid-cols-2 grid-rows-2 bg mt-[80px]  ml-[68px] mr-[68px]">
          <div
            className=" rounded-[20px] bg-[#FAFAFA] "
            data-aos="zoom-in-right"
          >
            <div className="flex ">
              <div className="m-10">
                <h2 className="Mavjud">
                  Mavjud <br />
                  mumammolar
                </h2>
              </div>
              <img
                className=" w-10 h-20 mt-10 h-[60px] ml-[131px] mt-[50px]"
                src={idumaloq}
                alt="#"
              />
            </div>

            <div className="mt-[54px] ml-10">
              <div className="flex">
                <img className='mt-5' src={idum} alt="#" />
                <p className="ml-[10px] mt-[19px]">Farzandingiz o’jarmi?</p>
              </div>

              <div className="flex">
                <img className="" src={idum} alt="#" />
                <p className="ml-[10px]">Husnixatidan qoniqmaysizmi?</p>
              </div>

              <div className="flex">
                <img className="" src={idum} alt="#" />
                <p className="ml-[10px]">Xotirasi sustmi?</p>
              </div>

              <div className="flex">
                <img className="" src={idum} alt="#" />
                <p className="ml-[10px]">
                  Yangi insonlar bilan tez chiqisha olmaydimi?
                </p>
              </div>
            </div>
          </div>
          <div className="rounded-[20px] bg-[#FAFAFA]" data-aos="zoom-in-left">
            <div className="flex ">
              <div className="m-10">
                <h2 className="muommolar">
                  Muammolarning <br />
                  bizdagi yechimi
                </h2>
              </div>
              <img
                className=" w-10 h-20 mt-10 h-[60px] ml-[104px] mt-[50px]"
                src={togri}
                alt="#"
              />
            </div>

            <div className="mt-[54px] ml-10">
              <div className="flex">
                <img src={i2} alt="#" />
                <p className="ml-[10px] mt-[19px]">
                  Bizdagi malakali ustozlarning birinchi darslarining o’zidayoq
                  farzandingizdagi o’zgarishlarni ko’rib lol qolasiz
                </p>
              </div>

              <div className="flex">
                <img className="-mt-4" src={i2} alt="#" />
                <p className="ml-[10px]">
                  Maxsus mashg’ulotlarimiz orqali farzandingiz husnixati
                  birnecha barobar yaxshilanadi va butun umri davomida chiroyli
                  husnixat bilan yoza oladi
                </p>
              </div>

              <div className="flex">
                <img className="" src={i2} alt="#" />
                <p className="ml-[10px]">
                  Interaktiv darslar yordamida farzandingiz xotirasi rivojlanadi
                </p>
              </div>

              <div className="flex mb-[66px]">
                <img className="" src={i2} alt="#" />
                <p className="ml-[10px]">
                  Bizdagi samimiy ustozlar va ahillik jamoa shiori ostida
                  harakat qiluvchi malakali mutaxassislarimiz farzandlaringizni,
                  yangi insonlar bilan tez chiqisha olishi uchun barcha
                  ko’nikmalarni sindirishadi
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Bolalarkurslari
