import React, { useState } from "react";
import Slider from "react-slick";
import { RiStarFill } from "react-icons/ri";
import { HiArrowRight, HiArrowLeft } from "react-icons/hi";
import ingliz from "../../assets/image 18 (1).png";
import vengrya from "../../assets/image 20.png";
import germaniya from "../../assets/Mask group.png";
import cat from "../../assets/images_gif/cat.gif";
import boy from "../../assets/images_gif/smart-boy.gif";
import cofe from "../../assets/images_gif/cofe.gif";
import olmaxon from "../../assets/images_gif/olmaxon.gif";
import yer from "../../assets/🌏 (1).svg";
import kitob from "../../assets/📚 (3).svg";
import bola from "../../assets/👶 (1).svg";
import frame5 from "../../assets/Group (16).svg";
import img1 from "../Diyorbek/section1_svg/img.svg";
import img2 from "../Diyorbek/section1_svg/opa (2).png";

function SampleNextArrow(props) {
  const { onClick } = props;
  return (
    <div
      className="w-14 h-12  hover:bg-white  duration-300 rounded-md text-2xl text-white-400 flex justify-center items-center absolute top-0 right-0 shadow-shadowOne cursor-pointer z-10"
      onClick={onClick}
    >
      <HiArrowRight />
    </div>
  );
}

function SamplePrevArrow(props) {
  const { onClick } = props;
  return (
    <div
      className="w-14 h-12  hover:bg-white duration-300 rounded-md text-2xl text-white-400 flex justify-center items-center absolute top-0 right-20 shadow-shadowOne cursor-pointer z-10"
      onClick={onClick}
    >
      <HiArrowLeft />
    </div>
  );
}

const Banner = () => {
  const [dotActive, setDocActive] = useState(0);
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    beforeChange: (prev, next) => {
      setDocActive(next);
    },
    appendDots: (dots) => (
      <div
        style={{
          borderRadius: "10px",
          padding: "10px",
        }}
      >
        <ul
          style={{
            display: "flex",
            gap: "15px",
            justifyContent: "center",
            marginTop: "20px",
          }}
        >
          {" "}
          {dots}{" "}
        </ul>
      </div>
    ),
    customPaging: (i) => (
      <div
        className="mb-[250px]"
        style={
          i === dotActive
            ? {
                width: "12px",
                height: "12px",
                color: "blue",
                background: "black",
                borderRadius: "50%",
                cursor: "pointer",
              }
            : {
                width: "12px",
                height: "12px",
                color: "blue",
                background: "gray",
                borderRadius: "50%",
                cursor: "pointer",
              }
        }
      ></div>
    ),
  };
  return (
    <section
      id="bizhaqimizda"
      className="banner w-full py-20 border-b-[1px] border-b-black"
    >
      <div className="mt-28">
        <div className="container-fuild ">
          <div className="flex justify-center gap-4 items-center pb-4 mt-[50px]">
            <h1 className="bizdan">Bizdan </h1>
            <img className="olmaxon" src={olmaxon} />
            <h1 className="bizdan">Siz uchun</h1>
          </div>
          <div className="til flex">
            <h1 className="oqish flex gap-4">
              <img src={kitob} />
              Xorijda o`qish
            </h1>
            <img className="cofe" src={cofe} />
            <h1 className="oqish flex gap-4">
              <img src={yer} />
              Til kurslari
            </h1>
          </div>
          <div className="kurs">
            <img className="cat" src={cat} />
            <h1 className="oqish flex gap-4">
              {" "}
              <img src={bola} />
              Bolalar kurslari
            </h1>{" "}
            <img className="boy" src={boy} />
          </div>
          <div className="svgs"></div>
        </div>
      </div>

      <div className="karusel flex justify-center items-center text-center">
        <h1 className="karuselh1">So`ngi yangiliklar</h1>
      </div>
      <div className="max-w-6xl mx-auto">
        {/* ================ Slider One ================== */}
        <Slider {...settings}>
          <div className="w-full">
            <div className="w-full h-auto flex flex-col lgl:flex-row justify-between">
              <div className="w-full lgl:w-[35%] h-full  p-8 rounded-lg shadow-shadowOne flex flex-col md:flex-row lgl:flex-col gap-8 justify-center md:justify-start lgl:justify-center">
                <img
                  className="h-72 md:h-32 lgl:h-72 rounded-lg object-cover"
                  src={ingliz}
                />

                <div className="w-full flex flex-col justify-end">
                  <p className="text-xs uppercase text-designColor tracking-wide mb-2">
                    12 Noy 2022 • Yangiliklar
                  </p>
                  <h3 className="text-2xl font-bold">
                    Linguaskill Ingliz tili testi bilan tez natijalar
                  </h3>
                  <p className="text-base tracking-wide text-black-500">
                    Operation Officer
                  </p>
                </div>
              </div>
              <div className="w-full lgl:w-[60%] h-full flex flex-col justify-between">
                <div className="w-full h-[70%] py-10 rounded-lg shadow-shadowOne p-4 lgl:p-8 flex flex-col justify-center gap-4 lgl:gap-8">
                  <div className="flex flex-col justify-between lgl:items-center py-6 border-b-2 border-b-gray-900">
                    <div>
                      <h3 className="text-xl lgl:text-2xl font-medium tracking-wide">
                        Linguaskill imtihoni Cambridge Assessment English
                        tomonidan tashkil qilinadigan onlayn imtihon turi
                        bo’lib, asosan ta’lim institutlari tomonidan talabalarni
                        baholash, til bilish darajalarini aniqlash, ish
                        beruvchilar uchun hodimlarining til bilish darajasini
                        aniqlash maqsadlarida tashkil qilinadi.
                      </h3>
                      <p className="text-base text-gray-400 mt-3">
                        via Upwork - Mar 4, 2015 - Aug 30, 2021 test
                      </p>
                    </div>
                    <div className="text-yellow-500 flex gap-1">
                      <RiStarFill />
                      <RiStarFill />
                      <RiStarFill />
                      <RiStarFill />
                      <RiStarFill />
                    </div>
                  </div>
                  <p className="text-base font-titleFont text-gray-400 font-medium tracking-wide leading-6">
                    Ushbu imtihon jami 4ta ko’nikma 3ta modulda o’tkaziladi:
                    Reading and Listening Speaking Writing Boshqa imtihonlardan
                    farqi imtihon to’liq kompyuterda topshiriladi. Test
                    natijalari 48 soat ichida ma’lum bo’ladi. Natijalarni
                    baholash CEFR bo’yicha bo’ladi va quyidagi ballar uchun
                    quyidagi darajalar beriladi: 100-119 = A1 (Beginner) daraja
                    120-139 = A2 (Elementary) daraja. 140 -159 = B1
                    (Pre-Intermediate – Intermediate) daraja. 160 -179 = B2
                    (Upper-Intermediate) daraja. 180 + = C1 (Advanced) darajaga
                    to’g’ri keladi. Sertifikat amal qilish muddati 2 yil.
                    Imtihondan C1 darajasi olinganda ingliz tili o’qituvchilari
                    oyligiga 50 foiz ustama to’lanishi belgilangan. Linguaskill
                    sizga qanday yordam berishi haqida ko'proq ma'lumot olish
                    uchun biz bilan aloqaga chiqing.
                  </p>
                </div>
              </div>
            </div>
          </div>
          {/* ================ Slider Two ================== */}

          <div className="w-full">
            <div className="w-full h-auto flex flex-col lgl:flex-row justify-between">
              <div className="w-full lgl:w-[35%] h-full  p-8 rounded-lg shadow-shadowOne flex flex-col md:flex-row lgl:flex-col gap-8 justify-center md:justify-start lgl:justify-center">
                <img
                  className="h-72 md:h-32 lgl:h-72 rounded-lg object-cover"
                  src={vengrya}
                />

                <div className="w-full flex flex-col justify-end">
                  <p className="text-xs uppercase text-designColor tracking-wide mb-2">
                    12 Noy 2022 • Yangiliklar
                  </p>
                  <h3 className="text-2xl font-bold">
                    Vengryada ta`lim olish imkoniyati!
                  </h3>
                  <p className="text-base tracking-wide text-gray-500">
                    Operation Officer
                  </p>
                </div>
              </div>
              <div className="w-full lgl:w-[60%] h-full flex flex-col justify-between">
                <div className="w-full h-[70%] py-10  rounded-lg shadow-shadowOne p-4 lgl:p-8 flex flex-col justify-center gap-4 lgl:gap-8">
                  <div className="flex flex-col justify-between lgl:items-center py-6 border-b-2 border-b-gray-900">
                    <div>
                      <h3 className="text-xl lgl:text-2xl font-medium tracking-wide">
                        Yevropada bepul ta’lim! Hech bir yosh bo’lmasa kerak
                        Yevropada ta’lim olishni va u yerda o’z hayotini
                        o’zgartirishni istaydigan. English Campus sizlarga
                        Yevropaning Vengriya davlatida bepul ta’lim olish
                        imkoniyatini taqdim etadi. Endi Yevropda ta’lim olish
                        muammo emas.
                      </h3>
                      <p className="text-base text-gray-400 mt-3">
                        via Upwork - Mar 4, 2015 - Aug 30, 2021 test
                      </p>
                    </div>
                    <div className="text-yellow-500 flex gap-1">
                      <RiStarFill />
                      <RiStarFill />
                      <RiStarFill />
                      <RiStarFill />
                      <RiStarFill />
                    </div>
                  </div>
                  <p className="text-base font-titleFont text-gray-400 font-medium tracking-wide leading-6">
                    Vengriyada 100% grant asosida talim oling talablar: - Maktab
                    attestati yoki kollej/litsey diplom - Mativatsion insholar
                    Imkoniyatlar: - Oyiga 120 EURO dan stipendiya - Bepul
                    yotoqhona va tibbiy sug`urta - Bepul oqish ( grant asosida)\
                    Yevropa bo’ylab sayoxat uchun viza (shengen visa) -
                    2-Kursdan amerikaga work and travel dasturi mavjud Ko’proq
                    ma’lumot olish uchun biz bilan aloqaga chiqing.
                  </p>
                </div>
              </div>
            </div>
          </div>
          {/* ================ Slider Three ================== */}

          <div className="w-full">
            <div className="w-full h-auto flex flex-col lgl:flex-row justify-between">
              <div className="w-full lgl:w-[35%] h-full  p-8 rounded-lg shadow-shadowOne flex flex-col md:flex-row lgl:flex-col gap-8 justify-center md:justify-start lgl:justify-center">
                <img
                  className="h-72 md:h-32 lgl:h-72 rounded-lg object-cover"
                  src={germaniya}
                />

                <div className="w-full flex flex-col justify-end">
                  <p className="text-xs uppercase text-designColor tracking-wide mb-2">
                    12 Noy 2022 • Yangiliklar
                  </p>
                  <h3 className="text-2xl font-bold">
                    Germaniyada work and trawel dasturi
                  </h3>
                  <p className="text-base tracking-wide text-gray-500">
                    Operation Officer
                  </p>
                </div>
              </div>
              <div className="w-full lgl:w-[60%] h-full flex flex-col justify-between">
                <div className="w-full h-[70%] py-10 rounded-lg shadow-shadowOne p-4 lgl:p-8 flex flex-col justify-center gap-4 lgl:gap-8">
                  <div className="flex flex-col justify-between lgl:items-center py-6 border-b-2 border-b-gray-900">
                    <div>
                      <h3 className="text-xl lgl:text-2xl font-medium tracking-wide">
                        Yevropada bepul ta’lim! Hech bir yosh bo’lmasa kerak
                        Yevropada ta’lim olishni va u yerda o’z hayotini
                        o’zgartirishni istaydigan. English Campus sizlarga
                        Yevropaning Vengriya davlatida bepul ta’lim olish
                        imkoniyatini taqdim etadi. Endi Yevropda ta’lim olish
                        muammo emas.
                      </h3>
                      <p className="text-base text-gray-400 mt-3">
                        via Upwork - Mar 4, 2015 - Aug 30, 2021 test
                      </p>
                    </div>
                    <div className="text-yellow-500 flex gap-1">
                      <RiStarFill />
                      <RiStarFill />
                      <RiStarFill />
                      <RiStarFill />
                      <RiStarFill />
                    </div>
                  </div>
                  <p className="text-base font-titleFont text-gray-400 font-medium tracking-wide leading-6">
                    Vengriyada 100% grant asosida talim oling talablar: - Maktab
                    attestati yoki kollej/litsey diplom - Mativatsion insholar
                    Imkoniyatlar: - Oyiga 120 EURO dan stipendiya - Bepul
                    yotoqhona va tibbiy sug`urta - Bepul oqish ( grant asosida)\
                    Yevropa bo’ylab sayoxat uchun viza (shengen visa) -
                    2-Kursdan amerikaga work and travel dasturi mavjud Ko’proq
                    ma’lumot olish uchun biz bilan aloqaga chiqing.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </Slider>
      </div>
      <div>
        <div>
          <div>
            <div className="tepa flex-wrap ">
              <div>
                <div
                  className="flex "
                  data-aos="fade-right"
                  data-aos-duration="2000"
                >
                  <img className="mt-10" src={img1} alt="#" />
                  <h2 className="h2_left bg-red-500 text-xl md:text-5xl lg:text-7xl xl:text-5xl">
                    Koreya davlati bo’yicha <br />
                    TOP universitetlarda
                    <br />
                    tahsil olish imkoni
                  </h2>
                </div>
                <div
                  className="flex"
                  data-aos="fade-left"
                  data-aos-duration="2000"
                >
                  <img className="mt-10" src={img1} alt="#" />
                  <h2 className="h2_left text-xl md:text-5xl lg:text-7xl xl:text-5xl">
                    Intensiv kurslar orqali
                    <br />
                    xorijiy tillarni, tez va
                    <br />
                    sifatli oʻrganish
                  </h2>
                </div>
                <div
                  className="flex"
                  data-aos="fade-right"
                  data-aos-duration="2000"
                >
                  <img className="mt-10" src={img1} alt="#" />
                  <h2 className="h2_left text-xl md:text-5xl lg:text-7xl xl:text-5xl">
                    Farzandlar kelajagi
                    <br />
                    uchun, harakatni
                    <br />
                    bugundan boshlash
                  </h2>
                </div>
              </div>
              <img
                className="img  w-[3000px] h-[400px] lgl:w-[500px]   lgl:h-[550px] mdl:mt-[90px]     "
                src={img2}
                alt="#"
                data-aos="zoom-out"
              />
            </div>
          </div>
          <h2
            className={"English md:br-hidden"}
            data-aos="fade-up"
            data-aos-duration="3000"
          >
            English Campus oʻquv markazi, oʻzidagi bir-necha <br /> yillik
            tajribasi va 1000 dan ortiq kelajakdagi yoʻlini <br /> qidirayotgan
            yosh yigit-qizlarga oʻz yordamini <br /> berishga ulgurdi.
          </h2>
        </div>
        <div>
          <div
            className={"ikki mdl:inline-flex"}
            data-aos="fade-down"
            data-aos-easing="linear"
            data-aos-duration="1500"
          >
            <img className="hidden mdl:inline-flex" src={frame5} />
            <p className={"ikki_p hidden mdl:inline-flex"}>
              Oʻz kasbining haqiqiy mutaxassislari <br />
              orqali tashkillashtirilgan. Biz uchun har <br /> bir
              oʻquvchimizning alohida qadri va <br /> oʻrni bor. Bizning shior
              esa Bir maqsad <br /> yoʻlida, birgalikda harakat qilish!
            </p>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Banner;
