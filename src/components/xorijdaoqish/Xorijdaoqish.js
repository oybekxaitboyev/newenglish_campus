import React, { useState } from 'react'
import Germaniya from './Germaniya'
import Korea from './Korea'
import flag from "../../assets/images_gif/korea.gif"
import nature from "../../assets/image 15.png";


const Xorijdaoqish = () => {
  const [koreaData, setKoreaData] = useState(true);
  const [germaniyaData, setGermaniyaData] = useState(false);
  return (
    <section id="xorijdaoqish" className="w-full h-auto ">
      <div className="tag  flex justify-center -mt-[300px] truncate">
        <h1>Xorijda</h1>
      </div>
      <div className="tag flex justify-center gap-4 items-center -mt-[80px]">
        <img className="truncate" src={flag} alt="truncate" />
        <h1>o`qish</h1>
      </div>

      <div
        className="nature flex justify-center rounded-2xl truncate"
        data-aos="flip-left"
        data-aos-easing="ease-out-cubic"
        data-aos-duration="2000"
      >
        <img className="truncate" src={nature} alt="" />
      </div>

      <div className="Work  bg-[#FAFAFA;] rounded-3xl mt-44 p-4 w-auto flex justify-center ">
        <ul className="workbtn gap-8 grid grid-cols-2 ">
          <li
            onClick={() => setKoreaData(true) & setGermaniyaData(false)}
            className="resumLi"
          >
            Janubiy Koreada o`qish
          </li>
          <li
            onClick={() => setKoreaData(false) & setGermaniyaData(true)}
            className="resumLi"
          >
            Germaniyada work and travel
          </li>
        </ul>
      </div>
      {koreaData && <Korea />}
      {germaniyaData && <Germaniya />}

      {/*<Korea/>*/}
      {/*<Germaniya/>*/}
    </section>
  );
}

export default Xorijdaoqish
