import React from 'react'

const Korea = () => {
  return (
    <div>
      <div>
        <div className="container w-full h-full lg:flex justify-center mt-12 m-auto rounded-3xl">
          <div
            className="xl:w-1/2 lg:w-[475px] h-auto w-11/12 bg-[#FAFAFA;] lg:ml-0 md:ml-8 ml-4 rounded-3xl"
            data-aos="fade-right"
          >
            <h1 className="xl:w-8/12 xl:text-3xl text-2xl xl:px-10 xl:ml-0 ml-8 pt-10 text-black">
              Janubiy Koreaning nufuzli Universitetlaridagi bakalavr dasturiga
              kirish
            </h1>
            <div className="flex xl:ml-14 ml-8 mt-6">
              <div className="mt-1">
                <svg
                  width={16}
                  height={16}
                  viewBox="0 0 16 16"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M8 16C12.3765 16 16 12.3686 16 8C16 3.62353 12.3686 0 7.99216 0C3.62353 0 0 3.62353 0 8C0 12.3686 3.63137 16 8 16ZM8 14.6667C4.29804 14.6667 1.34118 11.702 1.34118 8C1.34118 4.29804 4.2902 1.33333 7.99216 1.33333C11.6941 1.33333 14.6588 4.29804 14.6667 8C14.6745 11.702 11.702 14.6667 8 14.6667ZM7.92941 5.20784C8.50196 5.20784 8.94902 4.75294 8.94902 4.18824C8.94902 3.61569 8.50196 3.16078 7.92941 3.16078C7.36471 3.16078 6.9098 3.61569 6.9098 4.18824C6.9098 4.75294 7.36471 5.20784 7.92941 5.20784ZM6.62745 12.3137H9.81961C10.1412 12.3137 10.3922 12.0863 10.3922 11.7647C10.3922 11.4588 10.1412 11.2157 9.81961 11.2157H8.84706V7.22353C8.84706 6.8 8.63529 6.51765 8.23529 6.51765H6.76078C6.43922 6.51765 6.18824 6.76078 6.18824 7.06667C6.18824 7.38824 6.43922 7.61569 6.76078 7.61569H7.6V11.2157H6.62745C6.30588 11.2157 6.0549 11.4588 6.0549 11.7647C6.0549 12.0863 6.30588 12.3137 6.62745 12.3137Z"
                    fill="#303030"
                  />
                </svg>
              </div>
              <p className="pl-3 text-base text-black">
                6 ta bo’lim, 46 fakultet
              </p>
            </div>
            <div className=" xl:ml-14 ml-8 mt-12">
              <div className="flex">
                <div className="mt-2">
                  <svg
                    width={16}
                    height={16}
                    viewBox="0 0 16 16"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M8 16C12.3765 16 16 12.3686 16 8C16 3.62353 12.3686 0 7.99216 0C3.62353 0 0 3.62353 0 8C0 12.3686 3.63137 16 8 16ZM8 14.6667C4.29804 14.6667 1.34118 11.702 1.34118 8C1.34118 4.29804 4.2902 1.33333 7.99216 1.33333C11.6941 1.33333 14.6588 4.29804 14.6667 8C14.6745 11.702 11.702 14.6667 8 14.6667ZM7.13725 11.7255C7.39608 11.7255 7.61569 11.6 7.77255 11.3569L11.3569 5.71765C11.4431 5.56863 11.5451 5.38824 11.5451 5.21569C11.5451 4.87059 11.2314 4.64314 10.902 4.64314C10.7059 4.64314 10.5098 4.76078 10.3608 4.99608L7.10588 10.2118L5.56078 8.21961C5.37255 7.96863 5.2 7.89804 4.98039 7.89804C4.64314 7.89804 4.37647 8.17255 4.37647 8.52549C4.37647 8.6902 4.44706 8.86275 4.55686 9.01176L6.47059 11.3569C6.66667 11.6157 6.87843 11.7255 7.13725 11.7255Z"
                      fill="#303030"
                    />
                  </svg>
                </div>
                <h1 className="pl-3 text-xl text-black">Gumanitar fanlar</h1>
              </div>
              <p className="xl:w-8/12 w-10/12 ml-8 pt-2 text-black">
                Yoshlar tarbiyasi fakulteti Konsalting yuridik fakulteti
                Ma’muriy boshqaruv fakulteti Politsiya boshqaruvi fakulteti
                Ijtimoiy ta’minot fakulteti
              </p>
            </div>
            <div className=" xl:ml-14 ml-8 mt-7">
              <div className="flex">
                <div className="mt-2">
                  <svg
                    width={16}
                    height={16}
                    viewBox="0 0 16 16"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M8 16C12.3765 16 16 12.3686 16 8C16 3.62353 12.3686 0 7.99216 0C3.62353 0 0 3.62353 0 8C0 12.3686 3.63137 16 8 16ZM8 14.6667C4.29804 14.6667 1.34118 11.702 1.34118 8C1.34118 4.29804 4.2902 1.33333 7.99216 1.33333C11.6941 1.33333 14.6588 4.29804 14.6667 8C14.6745 11.702 11.702 14.6667 8 14.6667ZM7.13725 11.7255C7.39608 11.7255 7.61569 11.6 7.77255 11.3569L11.3569 5.71765C11.4431 5.56863 11.5451 5.38824 11.5451 5.21569C11.5451 4.87059 11.2314 4.64314 10.902 4.64314C10.7059 4.64314 10.5098 4.76078 10.3608 4.99608L7.10588 10.2118L5.56078 8.21961C5.37255 7.96863 5.2 7.89804 4.98039 7.89804C4.64314 7.89804 4.37647 8.17255 4.37647 8.52549C4.37647 8.6902 4.44706 8.86275 4.55686 9.01176L6.47059 11.3569C6.66667 11.6157 6.87843 11.7255 7.13725 11.7255Z"
                      fill="#303030"
                    />
                  </svg>
                </div>
                <h1 className="pl-3 text-xl text-black">
                  Global menejment bo’limi
                </h1>
              </div>
              <p className="xl:w-8/12 w-10/12 ml-8 pt-2 text-black">
                Menejment fakulteti Xalqaro savdo fakulteti Turizm fakulteti
                Moliyaviy menejment fakulteti Nia IT fakulteti moliya menejmenti
                Global madaniyat sanoati fakulteti Buxgalteriya fakulteti.
              </p>
            </div>
            <div className=" xl:ml-14 ml-8 mt-7">
              <div className="flex">
                <div className="mt-2">
                  <svg
                    width={16}
                    height={16}
                    viewBox="0 0 16 16"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M8 16C12.3765 16 16 12.3686 16 8C16 3.62353 12.3686 0 7.99216 0C3.62353 0 0 3.62353 0 8C0 12.3686 3.63137 16 8 16ZM8 14.6667C4.29804 14.6667 1.34118 11.702 1.34118 8C1.34118 4.29804 4.2902 1.33333 7.99216 1.33333C11.6941 1.33333 14.6588 4.29804 14.6667 8C14.6745 11.702 11.702 14.6667 8 14.6667ZM7.13725 11.7255C7.39608 11.7255 7.61569 11.6 7.77255 11.3569L11.3569 5.71765C11.4431 5.56863 11.5451 5.38824 11.5451 5.21569C11.5451 4.87059 11.2314 4.64314 10.902 4.64314C10.7059 4.64314 10.5098 4.76078 10.3608 4.99608L7.10588 10.2118L5.56078 8.21961C5.37255 7.96863 5.2 7.89804 4.98039 7.89804C4.64314 7.89804 4.37647 8.17255 4.37647 8.52549C4.37647 8.6902 4.44706 8.86275 4.55686 9.01176L6.47059 11.3569C6.66667 11.6157 6.87843 11.7255 7.13725 11.7255Z"
                      fill="#303030"
                    />
                  </svg>
                </div>
                <h1 className="pl-3 text-xl text-black">Aniq fanlar bo’limi</h1>
              </div>
              <p className="xl:w-8/12 w-11/12 ml-8 pt-2 text-black">
                Kimyoviy texnologiya fakulteti Oziq-ovqat va ovqatlanish
                fakulteti Atrof-muhitni muhofaza qilish fakulteti Hayotni
                taminlash tizimlari fakulteti auk Sport fakulteti Ijtimoiy va
                jismoniy tarbiya fakulteti Sport tibbiyoti fakulteti.
              </p>
            </div>
            <div className=" xl:ml-14 ml-8 mt-7">
              <div className="flex">
                <div className="mt-2">
                  <svg
                    width={16}
                    height={16}
                    viewBox="0 0 16 16"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M8 16C12.3765 16 16 12.3686 16 8C16 3.62353 12.3686 0 7.99216 0C3.62353 0 0 3.62353 0 8C0 12.3686 3.63137 16 8 16ZM8 14.6667C4.29804 14.6667 1.34118 11.702 1.34118 8C1.34118 4.29804 4.2902 1.33333 7.99216 1.33333C11.6941 1.33333 14.6588 4.29804 14.6667 8C14.6745 11.702 11.702 14.6667 8 14.6667ZM7.13725 11.7255C7.39608 11.7255 7.61569 11.6 7.77255 11.3569L11.3569 5.71765C11.4431 5.56863 11.5451 5.38824 11.5451 5.21569C11.5451 4.87059 11.2314 4.64314 10.902 4.64314C10.7059 4.64314 10.5098 4.76078 10.3608 4.99608L7.10588 10.2118L5.56078 8.21961C5.37255 7.96863 5.2 7.89804 4.98039 7.89804C4.64314 7.89804 4.37647 8.17255 4.37647 8.52549C4.37647 8.6902 4.44706 8.86275 4.55686 9.01176L6.47059 11.3569C6.66667 11.6157 6.87843 11.7255 7.13725 11.7255Z"
                      fill="#303030"
                    />
                  </svg>
                </div>
                <h1 className="pl-3 text-xl text-black">Politexnika bo’limi</h1>
              </div>
              <p className="xl:w-8/12 ml-8 pt-2 text-black xl:mr-0 mr-2">
                Kompyuter injiniringi fakulteti Fakultet Kompyuter fanlari
                axborot va aloqa Fakultet Elektron texnologiyalar fakulteti
                elektrotexnika va elektronika Fakultet Elektron axborot
                fakulteti kimyoviy texnologiya Fakultet nano texnologiya
                muhandisligi Fakultet Fakultet Mashinasozlik Axborot xavfsizligi
                fakulteti.
              </p>
            </div>
            <div className=" xl:ml-14 ml-8 mt-7">
              <div className="flex">
                <div className="mt-2">
                  <svg
                    width={16}
                    height={16}
                    viewBox="0 0 16 16"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M8 16C12.3765 16 16 12.3686 16 8C16 3.62353 12.3686 0 7.99216 0C3.62353 0 0 3.62353 0 8C0 12.3686 3.63137 16 8 16ZM8 14.6667C4.29804 14.6667 1.34118 11.702 1.34118 8C1.34118 4.29804 4.2902 1.33333 7.99216 1.33333C11.6941 1.33333 14.6588 4.29804 14.6667 8C14.6745 11.702 11.702 14.6667 8 14.6667ZM7.13725 11.7255C7.39608 11.7255 7.61569 11.6 7.77255 11.3569L11.3569 5.71765C11.4431 5.56863 11.5451 5.38824 11.5451 5.21569C11.5451 4.87059 11.2314 4.64314 10.902 4.64314C10.7059 4.64314 10.5098 4.76078 10.3608 4.99608L7.10588 10.2118L5.56078 8.21961C5.37255 7.96863 5.2 7.89804 4.98039 7.89804C4.64314 7.89804 4.37647 8.17255 4.37647 8.52549C4.37647 8.6902 4.44706 8.86275 4.55686 9.01176L6.47059 11.3569C6.66667 11.6157 6.87843 11.7255 7.13725 11.7255Z"
                      fill="#303030"
                    />
                  </svg>
                </div>
                <h1 className="pl-3 text-xl text-black">Tibbiyot bo’limi</h1>
              </div>
              <p className="xl:w-8/12 ml-8 pt-2 text-black xl:mr-0 mr-2">
                Sog’liqni saqlash fakulteti Sog’liqni saqlash va biotexnologiya
                fakulteti Tibbiyot PT muhandisligi fakulteti Tibbiyot
                texnologiyalari fakulteti Tibbiyot muhandisligi fakulteti.
              </p>
            </div>
            <div className=" xl:ml-14 ml-8 mt-7">
              <div className="flex">
                <div className="mt-2">
                  <svg
                    width={16}
                    height={16}
                    viewBox="0 0 16 16"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M8 16C12.3765 16 16 12.3686 16 8C16 3.62353 12.3686 0 7.99216 0C3.62353 0 0 3.62353 0 8C0 12.3686 3.63137 16 8 16ZM8 14.6667C4.29804 14.6667 1.34118 11.702 1.34118 8C1.34118 4.29804 4.2902 1.33333 7.99216 1.33333C11.6941 1.33333 14.6588 4.29804 14.6667 8C14.6745 11.702 11.702 14.6667 8 14.6667ZM7.13725 11.7255C7.39608 11.7255 7.61569 11.6 7.77255 11.3569L11.3569 5.71765C11.4431 5.56863 11.5451 5.38824 11.5451 5.21569C11.5451 4.87059 11.2314 4.64314 10.902 4.64314C10.7059 4.64314 10.5098 4.76078 10.3608 4.99608L7.10588 10.2118L5.56078 8.21961C5.37255 7.96863 5.2 7.89804 4.98039 7.89804C4.64314 7.89804 4.37647 8.17255 4.37647 8.52549C4.37647 8.6902 4.44706 8.86275 4.55686 9.01176L6.47059 11.3569C6.66667 11.6157 6.87843 11.7255 7.13725 11.7255Z"
                      fill="#303030"
                    />
                  </svg>
                </div>
                <h1 className="pl-3 text-xl text-black">
                  SCH media laboratoriyalari bo’limi
                </h1>
              </div>
              <p className="xl:w-8/12 ml-8 pt-2 text-black">
                Fakultet koreys tilini o’rganish Fakultet Koreyashunoslik
                Anglo-Amerika tadqiqotlari fakulteti Fakultet Sinologiya Media
                kommunikatsiyalari fakulteti Fakultet arxitektura Raqamli
                animatsiya fakulteti Narsalar interneti moddiy muhandislik
                fakulteti Fakultet Aqlli avtomobillar fakulteti Energetika
                tizimlari fakulteti Fakultet teatr sanati va multimedia
              </p>
            </div>
            <a>
              <div className="w-10/12 mx-10 mt-20 xl:mt-48 lg:mt-40 bg-white rounded-full h-16 flex justify-center items-center cursor-pointer">
                <p className="text-base flex items-center tracking-widest text-black mr-3 text-black">
                  Kursga yozilish
                </p>
                <div className="bg-gradient-to-r from-red to-gradientFinish text-transparent bg-red rounded-full">
                  <svg
                    width={32}
                    height={32}
                    viewBox="0 0 32 32"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <circle
                      cx={16}
                      cy={16}
                      r={16}
                      fill="url(#paint0_linear_106_1599)"
                    />
                    <path
                      fillRule="event"
                      clipRule="event"
                      d="M18.9175 15.3953L15.2798 11.8606L16.1161 11L20.8141 15.565C20.9303 15.6779 20.9959 15.8332 20.9959 15.9953C20.9959 16.1574 20.9303 16.3126 20.8141 16.4256L16.1161 20.9906L15.2798 20.1299L18.9175 16.5953H11V15.3953H18.9175Z"
                      fill="white"
                    />
                    <defs>
                      <linearGradient
                        id="paint0_linear_106_1599"
                        x1="1.5"
                        y1={9}
                        x2={30}
                        y2={26}
                        gradientUnits="userSpaceOnUse"
                      >
                        <stop offset="0.286458" stopColor="#B92032" />
                        <stop offset={1} stopColor="#C37BFC" />
                      </linearGradient>
                    </defs>
                  </svg>
                </div>
              </div>
            </a>
          </div>
          <div className="xl:w-1/2 lg:w-[580px] w-11/12 h-[] lg:ml-4 md:ml-8 ml-4 lg:mt-0 mt-4 rounded-3xl">
            <div
              className="bg-[#FAFAFA;] rounded-3xl xl:mt-0 lg:mt-0 md:mt-4 h-[540px]"
              data-aos="fade-down-left"
            >
              <h1 className="xl:w-7/12 text-3xl xl:pl-14 pl-8 pt-10 text-black ">
                Grantlar eng yaxshi talabalarga beriladi
              </h1>
              <div className="flex xl:ml-14 ml-8 mt-6">
                <div className="mt-1">
                  <svg
                    width={16}
                    height={16}
                    viewBox="0 0 16 16"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M8 16C12.3765 16 16 12.3686 16 8C16 3.62353 12.3686 0 7.99216 0C3.62353 0 0 3.62353 0 8C0 12.3686 3.63137 16 8 16ZM8 14.6667C4.29804 14.6667 1.34118 11.702 1.34118 8C1.34118 4.29804 4.2902 1.33333 7.99216 1.33333C11.6941 1.33333 14.6588 4.29804 14.6667 8C14.6745 11.702 11.702 14.6667 8 14.6667ZM7.92941 5.20784C8.50196 5.20784 8.94902 4.75294 8.94902 4.18824C8.94902 3.61569 8.50196 3.16078 7.92941 3.16078C7.36471 3.16078 6.9098 3.61569 6.9098 4.18824C6.9098 4.75294 7.36471 5.20784 7.92941 5.20784ZM6.62745 12.3137H9.81961C10.1412 12.3137 10.3922 12.0863 10.3922 11.7647C10.3922 11.4588 10.1412 11.2157 9.81961 11.2157H8.84706V7.22353C8.84706 6.8 8.63529 6.51765 8.23529 6.51765H6.76078C6.43922 6.51765 6.18824 6.76078 6.18824 7.06667C6.18824 7.38824 6.43922 7.61569 6.76078 7.61569H7.6V11.2157H6.62745C6.30588 11.2157 6.0549 11.4588 6.0549 11.7647C6.0549 12.0863 6.30588 12.3137 6.62745 12.3137Z"
                      fill="#303030"
                    />
                  </svg>
                </div>
                <p className="pl-3 text-base text-black">100% o’qish tolovi</p>
              </div>
              <div className="flex xl:ml-14 ml-8 mt-5">
                <div className="mt-1">
                  <svg
                    width={16}
                    height={16}
                    viewBox="0 0 16 16"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M8 16C12.3765 16 16 12.3686 16 8C16 3.62353 12.3686 0 7.99216 0C3.62353 0 0 3.62353 0 8C0 12.3686 3.63137 16 8 16ZM8 14.6667C4.29804 14.6667 1.34118 11.702 1.34118 8C1.34118 4.29804 4.2902 1.33333 7.99216 1.33333C11.6941 1.33333 14.6588 4.29804 14.6667 8C14.6745 11.702 11.702 14.6667 8 14.6667ZM7.92941 5.20784C8.50196 5.20784 8.94902 4.75294 8.94902 4.18824C8.94902 3.61569 8.50196 3.16078 7.92941 3.16078C7.36471 3.16078 6.9098 3.61569 6.9098 4.18824C6.9098 4.75294 7.36471 5.20784 7.92941 5.20784ZM6.62745 12.3137H9.81961C10.1412 12.3137 10.3922 12.0863 10.3922 11.7647C10.3922 11.4588 10.1412 11.2157 9.81961 11.2157H8.84706V7.22353C8.84706 6.8 8.63529 6.51765 8.23529 6.51765H6.76078C6.43922 6.51765 6.18824 6.76078 6.18824 7.06667C6.18824 7.38824 6.43922 7.61569 6.76078 7.61569H7.6V11.2157H6.62745C6.30588 11.2157 6.0549 11.4588 6.0549 11.7647C6.0549 12.0863 6.30588 12.3137 6.62745 12.3137Z"
                      fill="#303030"
                    />
                  </svg>
                </div>
                <p className="pl-3 text-base text-black">
                  100% yotoqxona to’lovi
                </p>
              </div>
              <div className="flex xl:ml-14 ml-8 mt-12">
                <div className="mt-1">
                  <svg
                    width={16}
                    height={16}
                    viewBox="0 0 16 16"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M8 16C12.3765 16 16 12.3686 16 8C16 3.62353 12.3686 0 7.99216 0C3.62353 0 0 3.62353 0 8C0 12.3686 3.63137 16 8 16ZM8 14.6667C4.29804 14.6667 1.34118 11.702 1.34118 8C1.34118 4.29804 4.2902 1.33333 7.99216 1.33333C11.6941 1.33333 14.6588 4.29804 14.6667 8C14.6745 11.702 11.702 14.6667 8 14.6667ZM7.13725 11.7255C7.39608 11.7255 7.61569 11.6 7.77255 11.3569L11.3569 5.71765C11.4431 5.56863 11.5451 5.38824 11.5451 5.21569C11.5451 4.87059 11.2314 4.64314 10.902 4.64314C10.7059 4.64314 10.5098 4.76078 10.3608 4.99608L7.10588 10.2118L5.56078 8.21961C5.37255 7.96863 5.2 7.89804 4.98039 7.89804C4.64314 7.89804 4.37647 8.17255 4.37647 8.52549C4.37647 8.6902 4.44706 8.86275 4.55686 9.01176L6.47059 11.3569C6.66667 11.6157 6.87843 11.7255 7.13725 11.7255Z"
                      fill="#303030"
                    />
                  </svg>
                </div>
                <p className="pl-3 text-base text-black">
                  Xalqaro talabalar uchun stipendiyalar
                </p>
              </div>
              <div className="flex xl:ml-14 ml-8 mt-5">
                <div className="mt-1">
                  <svg
                    width={16}
                    height={16}
                    viewBox="0 0 16 16"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M8 16C12.3765 16 16 12.3686 16 8C16 3.62353 12.3686 0 7.99216 0C3.62353 0 0 3.62353 0 8C0 12.3686 3.63137 16 8 16ZM8 14.6667C4.29804 14.6667 1.34118 11.702 1.34118 8C1.34118 4.29804 4.2902 1.33333 7.99216 1.33333C11.6941 1.33333 14.6588 4.29804 14.6667 8C14.6745 11.702 11.702 14.6667 8 14.6667ZM7.13725 11.7255C7.39608 11.7255 7.61569 11.6 7.77255 11.3569L11.3569 5.71765C11.4431 5.56863 11.5451 5.38824 11.5451 5.21569C11.5451 4.87059 11.2314 4.64314 10.902 4.64314C10.7059 4.64314 10.5098 4.76078 10.3608 4.99608L7.10588 10.2118L5.56078 8.21961C5.37255 7.96863 5.2 7.89804 4.98039 7.89804C4.64314 7.89804 4.37647 8.17255 4.37647 8.52549C4.37647 8.6902 4.44706 8.86275 4.55686 9.01176L6.47059 11.3569C6.66667 11.6157 6.87843 11.7255 7.13725 11.7255Z"
                      fill="#303030"
                    />
                  </svg>
                </div>
                <p className="xl:w-7/12 w-10/12 pl-3 text-base text-black">
                  TOPIK 3 va undan yuqori darajaga ega bo’lganlar uchun (imtihon
                  (Politexnika universitetiga abituriyentlar uchun - 2-bosqich)
                  Transfer talabalari uchun istisno.
                </p>
              </div>
              <a>
                <div className="w-10/12 mx-10    mt-10 bg-white rounded-full h-16 flex justify-center items-center cursor-pointer">
                  <p className="text-base flex items-center tracking-widest text-black mr-3 text-black">
                    Kursga yozilish
                  </p>
                  <div className=" bg-gradient-to-r from-red to-gradientFinish text-transparent bg-red rounded-full">
                    <svg
                      width={32}
                      height={32}
                      viewBox="0 0 32 32"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <circle
                        cx={16}
                        cy={16}
                        r={16}
                        fill="url(#paint0_linear_106_1599)"
                      />
                      <path
                        fillRule="event"
                        clipRule="event"
                        d="M18.9175 15.3953L15.2798 11.8606L16.1161 11L20.8141 15.565C20.9303 15.6779 20.9959 15.8332 20.9959 15.9953C20.9959 16.1574 20.9303 16.3126 20.8141 16.4256L16.1161 20.9906L15.2798 20.1299L18.9175 16.5953H11V15.3953H18.9175Z"
                        fill="white"
                      />
                      <defs>
                        <linearGradient
                          id="paint0_linear_106_1599"
                          x1="1.5"
                          y1={9}
                          x2={30}
                          y2={26}
                          gradientUnits="userSpaceOnUse"
                        >
                          <stop offset="0.286458" stopColor="#B92032" />
                          <stop offset={1} stopColor="#C37BFC" />
                        </linearGradient>
                      </defs>
                    </svg>
                  </div>
                </div>
              </a>
            </div>
            <div
              className="h-auto bg-[#FAFAFA;] rounded-3xl mt-4"
              data-aos="fade-up-left"
            >
              <h1 className="xl:w-7/12 text-3xl xl:pl-14 pl-8 pt-12 text-black font-bold">
                Bakalavr bosqichiga qabul qilish mezonlari
              </h1>
              <div className="flex xl:pl-14 pl-8 mt-8">
                <div className="mt-1">
                  <svg
                    width={16}
                    height={16}
                    viewBox="0 0 16 16"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M8 16C12.3765 16 16 12.3686 16 8C16 3.62353 12.3686 0 7.99216 0C3.62353 0 0 3.62353 0 8C0 12.3686 3.63137 16 8 16ZM8 14.6667C4.29804 14.6667 1.34118 11.702 1.34118 8C1.34118 4.29804 4.2902 1.33333 7.99216 1.33333C11.6941 1.33333 14.6588 4.29804 14.6667 8C14.6745 11.702 11.702 14.6667 8 14.6667ZM7.92941 5.20784C8.50196 5.20784 8.94902 4.75294 8.94902 4.18824C8.94902 3.61569 8.50196 3.16078 7.92941 3.16078C7.36471 3.16078 6.9098 3.61569 6.9098 4.18824C6.9098 4.75294 7.36471 5.20784 7.92941 5.20784ZM6.62745 12.3137H9.81961C10.1412 12.3137 10.3922 12.0863 10.3922 11.7647C10.3922 11.4588 10.1412 11.2157 9.81961 11.2157H8.84706V7.22353C8.84706 6.8 8.63529 6.51765 8.23529 6.51765H6.76078C6.43922 6.51765 6.18824 6.76078 6.18824 7.06667C6.18824 7.38824 6.43922 7.61569 6.76078 7.61569H7.6V11.2157H6.62745C6.30588 11.2157 6.0549 11.4588 6.0549 11.7647C6.0549 12.0863 6.30588 12.3137 6.62745 12.3137Z"
                      fill="#303030"
                    />
                  </svg>
                </div>
                <p className="pl-3 text-base text-black">
                  Talaba va uning ota-onasi chet ellik bo’lishi kerak
                </p>
              </div>
              <div className="flex xl:pl-14 pl-8 mt-8">
                <div className="mt-1">
                  <svg
                    width={16}
                    height={16}
                    viewBox="0 0 16 16"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M8 16C12.3765 16 16 12.3686 16 8C16 3.62353 12.3686 0 7.99216 0C3.62353 0 0 3.62353 0 8C0 12.3686 3.63137 16 8 16ZM8 14.6667C4.29804 14.6667 1.34118 11.702 1.34118 8C1.34118 4.29804 4.2902 1.33333 7.99216 1.33333C11.6941 1.33333 14.6588 4.29804 14.6667 8C14.6745 11.702 11.702 14.6667 8 14.6667ZM7.92941 5.20784C8.50196 5.20784 8.94902 4.75294 8.94902 4.18824C8.94902 3.61569 8.50196 3.16078 7.92941 3.16078C7.36471 3.16078 6.9098 3.61569 6.9098 4.18824C6.9098 4.75294 7.36471 5.20784 7.92941 5.20784ZM6.62745 12.3137H9.81961C10.1412 12.3137 10.3922 12.0863 10.3922 11.7647C10.3922 11.4588 10.1412 11.2157 9.81961 11.2157H8.84706V7.22353C8.84706 6.8 8.63529 6.51765 8.23529 6.51765H6.76078C6.43922 6.51765 6.18824 6.76078 6.18824 7.06667C6.18824 7.38824 6.43922 7.61569 6.76078 7.61569H7.6V11.2157H6.62745C6.30588 11.2157 6.0549 11.4588 6.0549 11.7647C6.0549 12.0863 6.30588 12.3137 6.62745 12.3137Z"
                      fill="#303030"
                    />
                  </svg>
                </div>
                <p className="w-4/6 pl-3 text-base text-black">
                  Talaba 12 yillik ta’limni tamomlashi kerak (maktab - 9 yil,
                  kollej yoki litsey - 3 yil)
                </p>
              </div>
              <div className="flex xl:pl-14 pl-8 mt-8">
                <div className="mt-1">
                  <svg
                    width={16}
                    height={16}
                    viewBox="0 0 16 16"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M8 16C12.3765 16 16 12.3686 16 8C16 3.62353 12.3686 0 7.99216 0C3.62353 0 0 3.62353 0 8C0 12.3686 3.63137 16 8 16ZM8 14.6667C4.29804 14.6667 1.34118 11.702 1.34118 8C1.34118 4.29804 4.2902 1.33333 7.99216 1.33333C11.6941 1.33333 14.6588 4.29804 14.6667 8C14.6745 11.702 11.702 14.6667 8 14.6667ZM7.92941 5.20784C8.50196 5.20784 8.94902 4.75294 8.94902 4.18824C8.94902 3.61569 8.50196 3.16078 7.92941 3.16078C7.36471 3.16078 6.9098 3.61569 6.9098 4.18824C6.9098 4.75294 7.36471 5.20784 7.92941 5.20784ZM6.62745 12.3137H9.81961C10.1412 12.3137 10.3922 12.0863 10.3922 11.7647C10.3922 11.4588 10.1412 11.2157 9.81961 11.2157H8.84706V7.22353C8.84706 6.8 8.63529 6.51765 8.23529 6.51765H6.76078C6.43922 6.51765 6.18824 6.76078 6.18824 7.06667C6.18824 7.38824 6.43922 7.61569 6.76078 7.61569H7.6V11.2157H6.62745C6.30588 11.2157 6.0549 11.4588 6.0549 11.7647C6.0549 12.0863 6.30588 12.3137 6.62745 12.3137Z"
                      fill="#303030"
                    />
                  </svg>
                </div>
                <p className=" pl-3 text-base text-black">
                  Topik 3 yoki undan yuqori darajaga ega bo’lishi kerak
                </p>
              </div>
              <div className="flex w-full xl:pl-14 pl-8 mt-12">
                <div className="mt-1">
                  <svg
                    width={16}
                    height={16}
                    viewBox="0 0 16 16"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M8 16C12.3765 16 16 12.3686 16 8C16 3.62353 12.3686 0 7.99216 0C3.62353 0 0 3.62353 0 8C0 12.3686 3.63137 16 8 16ZM8 14.6667C4.29804 14.6667 1.34118 11.702 1.34118 8C1.34118 4.29804 4.2902 1.33333 7.99216 1.33333C11.6941 1.33333 14.6588 4.29804 14.6667 8C14.6745 11.702 11.702 14.6667 8 14.6667ZM7.13725 11.7255C7.39608 11.7255 7.61569 11.6 7.77255 11.3569L11.3569 5.71765C11.4431 5.56863 11.5451 5.38824 11.5451 5.21569C11.5451 4.87059 11.2314 4.64314 10.902 4.64314C10.7059 4.64314 10.5098 4.76078 10.3608 4.99608L7.10588 10.2118L5.56078 8.21961C5.37255 7.96863 5.2 7.89804 4.98039 7.89804C4.64314 7.89804 4.37647 8.17255 4.37647 8.52549C4.37647 8.6902 4.44706 8.86275 4.55686 9.01176L6.47059 11.3569C6.66667 11.6157 6.87843 11.7255 7.13725 11.7255Z"
                      fill="#303030"
                    />
                  </svg>
                </div>
                <p className="xl:w-8/12 w-11/12 pl-3 text-base text-black">
                  Fakultetlar Gumanitar fanlar universiteti Ingliz tili
                  fakulteti Xitoy tili fakulteti Global erkin fanlar fakulteti
                  Koreya madaniy mazmuni
                  <br className="hidden xl:block" />
                  fakulteti Global menejment universiteti Hayot fanlari
                  universiteti Tibbiyot muhandisligi fakulteti Ommaviy
                  kommunikatsiyalar fakulteti Jamoat salomatligi fakulteti Media
                  kommunikatsiyalar
                  <br className="hidden xl:block" />
                  fakulteti Aktyorlik va raqs fakulteti Kino va animatsiya
                  fakulteti Elektron tijorat fakulteti Politexnika universiteti
                  Tibbiyot IT
                  <br className="hidden xl:block" />
                  muhandisligi fakulteti Materiallar muhandisligi fakulteti
                  raqamli animatsiya Internet tadqiqotlari fakulteti Aqlli
                  avtomobillar
                  <br className="hidden xl:block" />
                  fakulteti Energetika tizimlari fakulteti Ijro sanati fakulteti
                  Tibbiyot muhandisligi fakulteti Arxitektura fakulteti (5 yil)
                  Sog’liqni saqlash
                  <br className="hidden xl:block" />
                  va biotexnologiya fakulteti Yoshlar ta’limi konsalting
                  fakulteti Ijtimoiy ta’minot fakulteti
                </p>
              </div>
              <a>
                <div className="w-10/12 mx-10 2xl:mt-24 xl:mt-40 mt-10 bg-white rounded-full h-16 flex justify-center items-center cursor-pointer">
                  <p className="text-base flex items-center  tracking-widest text-black mr-3 text-black">
                    Kursga yozilish
                  </p>
                  <div
                    className=" bg-gradient-to-r from-red to-gradientFinish text-transparent bg-red rounded-full"
                    id="child-courses"
                  >
                    <svg
                      width={32}
                      height={32}
                      viewBox="0 0 32 32"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <circle
                        cx={16}
                        cy={16}
                        r={16}
                        fill="url(#paint0_linear_106_1599)"
                      />
                      <path
                        fillRule="event"
                        clipRule="event"
                        d="M18.9175 15.3953L15.2798 11.8606L16.1161 11L20.8141 15.565C20.9303 15.6779 20.9959 15.8332 20.9959 15.9953C20.9959 16.1574 20.9303 16.3126 20.8141 16.4256L16.1161 20.9906L15.2798 20.1299L18.9175 16.5953H11V15.3953H18.9175Z"
                        fill="white"
                      />
                      <defs>
                        <linearGradient
                          id="paint0_linear_106_1599"
                          x1="1.5"
                          y1={9}
                          x2={30}
                          y2={26}
                          gradientUnits="userSpaceOnUse"
                        >
                          <stop offset="0.286458" stopColor="#B92032" />
                          <stop offset={1} stopColor="#C37BFC" />
                        </linearGradient>
                      </defs>
                    </svg>
                  </div>
                </div>
              </a>
              <h1 className="text-transparent mt-10">Kursga yozilish</h1>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Korea
