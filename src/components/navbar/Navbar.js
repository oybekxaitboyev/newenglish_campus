import React, {useState} from 'react'
import Modal from 'react-modal';
import { FiMenu } from "react-icons/fi";
import { MdClose } from "react-icons/md";
import { FaYoutube, FaFacebook, FaInstagram, FaTelegram } from "react-icons/fa";
import { Link } from 'react-scroll'
import logo from '../../assets/Group 109 (3).svg'
import { navLinksdata } from '../../constants'
import kitob2 from '../../assets/📚 (4).svg'
import bola2 from '../../assets/🌏 (2).svg'
import yer2 from '../../assets/👶 (2).svg'
import gb from '../../assets/🇬🇧 (1).svg'
import kr from '../../assets/🇰🇷 (1).svg'
import ru from '../../assets/🇷🇺 (1).svg'

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
};




const Navbar = () => {
  let subtitle;
  const [modalIsOpen, setIsOpen] = React.useState(false);
  
  function openModal() {
    setIsOpen(true);
  }
  
  function afterOpenModal() {
    // references are now sync'd and can be accessed.
    subtitle.style.color = '#f00';
  }
  
  function closeModal() {
    setIsOpen(false);
  }
  
  const [showMenu, setShowMenu]=useState(false)
  
  return (
    <div className="w-full h-24 mx-auto bg-white flex justify-around items-center fixed z-10">
      <div>
        <img src={logo} />
      </div>
      <div>
        <ul className="hidden mdl:inline-flex items-center gap-6 lg:gap-10">
          {navLinksdata.map(({ _id, title, link }) => (
            <li
              data-aos="fade-down"
              data-aos-duration="3000"
              className="navtext text-bold text-black tracking-wide cursor-pointer
            hover:text-[#B92032] duration-300"
              key={_id}
            >
              <Link
                activeClass="active"
                to={link}
                spy={true}
                smooth={true}
                offset={-70}
                duration={500}
              >
                {title}
              </Link>
            </li>
          ))}
        </ul>
      </div>

      {showMenu && (
        <div className="w-[100%] h-screen  absolute top-0 left-0 bg-white p-4 scrollbar-hide">
          <div className="flex flex-col gap-8 py-2 relative">
            <div>
              <img className="w-32" src={logo} alt="logo" />
            </div>
            <ul className="flex flex-col gap-4 mt-28">
              {navLinksdata.map((item) => (
                <li
                  key={item._id}
                  className="text-base font-normal text-black-400 tracking-wide text-center cursor-pointer hover:text-[#B92032] duration-300"
                >
                  <Link
                    onClick={() => setShowMenu(false)}
                    activeClass="active"
                    to={item.link}
                    spy={true}
                    smooth={true}
                    offset={-70}
                    duration={500}
                  >
                    {item.title}
                  </Link>
                </li>
              ))}
            </ul>
            <div className="flex flex-col gap-4">
              <div className="footer">
                <a href="https://www.facebook.com/home.php" target="_blank">
                  <FaFacebook className="icon" />
                </a>
                <a href="https://www.instagram.com/" target="_blank">
                  <FaInstagram className="icon" />
                </a>
                <a href="https://t.me/english_campus_bot" target="_blank">
                  <FaTelegram className="icon" />
                </a>
                <a href="https://www.youtube.com/" target="_blank">
                  <FaYoutube className="icon" />
                </a>
              </div>
              <p className="text-center">
                © 2022 English Campus. All rights reserved.
              </p>
            </div>
            <span
              onClick={() => setShowMenu(false)}
              className="absolute top-4 right-4 text-[#B92032] hover:text-designColor duration-300 text-2xl cursor-pointer"
            >
              <MdClose />
            </span>
          </div>
        </div>
      )}

      <div>
        <button
          className="hidden mdl:inline-flex openmodal text-base text-black tracking-wide cursor-pointer
            hover:text-[#B92032] duration-300"
          onClick={openModal}
        >
          Kursga yozilish
        </button>
        <Modal
          isOpen={modalIsOpen}
          onAfterOpen={afterOpenModal}
          onRequestClose={closeModal}
          style={customStyles}
          contentLabel="Example Modal"
        >
          <h2 ref={(_subtitle) => (subtitle = _subtitle)}></h2>
          <div className="flex justify-around">
            <h1 className="modalh1">Kurs uchun Ariza yuborish</h1>
            <button className="modalbtn gap-4" onClick={closeModal}>
              {" "}
              <i class="fa fa-times" aria-hidden="true"></i>Oynani yopish
            </button>
          </div>

          <p className="modal-p">Bo`limni tanlang</p>
          <div className="flex">
            <button className="modalbtn flex gap-2">
              <img src={kitob2} />
              Til kurslari
            </button>
            <button className="modalbtn flex gap-2">
              <img src={yer2} />
              Xorijda o`qish
            </button>
            <button className="modalbtn flex gap-2">
              <img src={bola2} />
              Bolalar kurslari
            </button>
          </div>
          <p className="modal-p">Ichki bo`limni tanlang</p>
          <div className="flex ">
            <button className="modalbtn flex gap-2">
              <img src={gb} />
              Ingliz tili{" "}
            </button>
            <button className="modalbtn flex gap-2">
              <img src={ru} />
              Rus tili
            </button>
            <button className="modalbtn flex gap-2">
              <img src={kr} />
              Koreys tili
            </button>
          </div>
          <p className="modal-p">O`zingiz haqingizda</p>
          <form>
            <div className="flex justify-around">
              <input className="input" type="text" placeholder={"Ismingiz"} />

              <input
                className="input"
                type="tel"
                placeholder={"Telefon raqamingiz"}
              />
            </div>

            <div className="flex justify-center">
              <textarea
                className="input-t"
                placeholder={"Xabaringizni shu yerga yozing..."}
              />
            </div>
          </form>
          <div className="flex justify-center pt-4">
            <button className="aloqabtn">Kurs uchun ariza yuborish</button>
          </div>
        </Modal>
      </div>
      <span
        onClick={() => setShowMenu(!showMenu)}
        className="text-2xl mdl:hidden  inline-flex items-center justify-center pl-[40px] text-[#B92032] cursor-pointer"
      >
        <FiMenu />
      </span>
    </div>
  );
} 

export default Navbar;
