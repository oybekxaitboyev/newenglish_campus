import React, { useEffect, useState } from 'react';
import { FaYoutube, FaFacebook, FaInstagram, FaTelegram } from "react-icons/fa";
import baby from '../../assets/images_gif/childPhone.gif'

{/*import { img1, img2 } from '../img'; */}


const Aloqa = () => {
 
  const [name, setName] = useState("");
  const [phone, setPhone] = useState("");
  const [text, setText] = useState("");
  let url = `https://api.telegram.org/bot6004747091:AAEPHQcyYt_zvc2OBkSJ6UqBCiTCEhk7ZQQ/sendMessage?chat_id=1197709139&text=%0A-Isim: ${name}, %0A- Raqam: ${phone},  %0A- Matin: ${text}, %0A`;
  function sendMassageFunction() {
    fetch(url).then((res) => res.json());
  }

  return (
    <section
      id="aloqa"
      className='aloqa w-full h-auto pb-20 flex justify-center data-aos="flip-down"'
    >
      <div className={"contact"} data-aos="flip-up" data-aos-duration="1500">
        <div className="tag">
          <h1>Aloqaga</h1>
          <div className="tag2">
            <img src={baby} />
            <h1>chiqish</h1>
          </div>
        </div>
        <div className="arrow"></div>
        <form>
          <input
            defaultValue={name}
            onChange={(e) => setName(e.target.value)}
            id="name"
            type="text"
            placeholder={"Ismingiz"}
          />

          <input
            defaultValue={phone}
            onChange={(e) => setPhone(e.target.value)}
            id={"phone"}
            type="tel"
            placeholder={"Telefon raqamingiz"}
          />

          <textarea
            defaultValue={text}
            onChange={(e) => setText(e.target.value)}
            id={"text"}
            placeholder={"Xabaringizni shu yerga yozing..."}
          />
          <button
            className="aloqabtn"
            onClick={sendMassageFunction}
            type="submit"
          >
            Xabarni yuborish
          </button>
        </form>
        <div className="footer">
          <a href="https://www.facebook.com/home.php" target="_blank">
            <FaFacebook className="icon" />
          </a>
          <a href="https://www.instagram.com/" target="_blank">
            <FaInstagram className="icon" />
          </a>
          <a href="https://t.me/english_campus_bot" target="_blank">
            <FaTelegram className="icon" />
          </a>
          <a href="https://www.youtube.com/" target="_blank">
            <FaYoutube className="icon" />
          </a>
        </div>
        <p className="aloqad">© 2022 English Campus. All rights reserved.</p>
      </div>
    </section>
  );
}

export default Aloqa
