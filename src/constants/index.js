export const navLinksdata = [
    {
      _id: 1001,
      title: "Biz haqimizda",
      link: "bizhaqimizda",
    },
    {
      _id: 1002,
      title: "Til kurslari",
      link: "tilkurslari",
    },
    {
      _id: 1003,
      title: "Xorijda o`qish",
      link: "xorijdaoqish",
    },
    {
      _id: 1004,
      title: "Bolalar kurslari",
      link: "bolalarkurslari",
    },
    {
      _id: 1005,
      title: "Aloqa",
      link: "aloqa",
    },
  ];