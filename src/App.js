import Banner from "./components/banner/Banner";
import Navbar from "./components/navbar/Navbar";
import React from "react";
import "./index.css";
import "./components/banner/banner.css";
import "./components/aloqa/Aloqa.css";
import Kolalarkurslari from "./components/bolalarkurslari/Kolalarkurslari.css";
import Tilkurslari from "./components/tilkurslari/Tilkurslari";
import Xorijdaoqish from "./components/xorijdaoqish/Xorijdaoqish";
import Bolalarkurslari from "./components/bolalarkurslari/Bolalarkurslari";
import Aloqa from "./components/aloqa/Aloqa";
import AOS from "aos";
import "aos/dist/aos.css";
import { useEffect } from "react";

function App() {
  useEffect(() => {
    AOS.init({});
  });
  return (
    <div className="w-full h-auto">
      <div className="max-w-screen-2xl mx-auto overflow-x-hidden">
        <Navbar />
        <Banner />
        <Tilkurslari />
        <Xorijdaoqish />
        <Bolalarkurslari />
        <Aloqa />
      </div>
    </div>
  );
}

export default App;
